#!/bin/bash
set -u

basedir=$(realpath $(dirname $0))

ca() {
  (
  cd ${basedir}/etc &&
  rm -f ca* &&
  ${basedir}/bin/pki.sh ca1 &&
  echo "ca() done"
  )
}

image() {
  local name=${1}; shift
  local path="jrevolt/citools"
  local version="latest"
  local registry=${registry:-registry.gitlab.com}
  local cache=${cache:-$registry}
  local push=${push:-true}
  docker run -it --rm --privileged \
    -w /work \
    -v "$(cygpath -wam ${basedir}):/work" \
    -v "$(cygpath -wam ~/.docker/auth.json):/home/user/.docker/config.json" \
    -v jrevolt-citools:/home/user/.local/share/buildkit \
    --entrypoint buildctl-daemonless.sh \
    moby/buildkit:rootless \
      build \
        --frontend dockerfile.v0 \
        --local context=. \
        --local dockerfile=. \
        --import-cache=type=registry,ref=${cache}/${path}/{redis,gitversion,router,runner,buildkit}:cache \
        --import-cache=type=registry,ref=${cache}/${path}/${name}:cache \
        --export-cache=type=registry,ref=${cache}/${path}/${name}:cache \
        --output type=image,name=${registry}/${path}/${name}:${version},push=${push} \
        $([[ ${name} != "all" ]] && echo "--opt target=${name}") \
        "$@"

}

images() {
  local names="${@:-redis gitversion router runner buildkit}"
  for i in ${names}; do image $i;  done && echo "Built: ${names}"
}

stop1() {
  helm -n ci ls -q | grep buildkit | xargs -r helm -n ci uninstall
  kubectl -n ci scale --replicas=0 sts/{router,runner}
}

"$@"
