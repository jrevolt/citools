# syntax=docker/dockerfile:1-experimental

ARG GITLAB_RUNNER_VERSION=12.9.0
ARG BUILDKIT_VERSION=0.7.1
ARG KUBECTL_VERSION=1.18.0
ARG HELM_VERSION=3.2.0
ARG GITVERSION_VERSION=5.3.3
ARG CITOOLS_HOME=/opt/jrevolt/citools

FROM alpine as download-gitlab-runner
ARG GITLAB_RUNNER_VERSION
RUN echo "Downloading gitlab-runner: ${GITLAB_RUNNER_VERSION}" &&\
    fname="/usr/local/bin/gitlab-runner" &&\
    url="https://s3.amazonaws.com/gitlab-runner-downloads/v${GITLAB_RUNNER_VERSION}/binaries/gitlab-runner-linux-386" &&\
    echo "$url" &&\
    wget -O $fname $url &&\
    chmod +x $fname

FROM alpine as download-kubectl
ARG KUBECTL_VERSION
RUN echo "Downloading kubectl: ${KUBECTL_VERSION}" &&\
    fname="/usr/local/bin/kubectl" &&\
    url="https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl" &&\
    echo "$url" &&\
    wget -O $fname $url &&\
    chmod +x $fname

FROM alpine as download-helm
ARG HELM_VERSION
RUN echo "Downloading helm: ${HELM_VERSION}" &&\
    fname="/usr/local/bin/helm" &&\
    url="https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz" &&\
    echo "$url" &&\
    wget -O - $url | tar xz -C /usr/local/bin linux-amd64/helm --strip-components=1  &&\
    chmod +x $fname &&\
    helm version

FROM alpine as download-gitversion
ARG GITVERSION_VERSION
RUN echo "Downloading gitversion: ${GITVERSION_VERSION}" &&\
    fname="/usr/local/bin/gitversion" &&\
    url="https://github.com/GitTools/GitVersion/releases/download/${GITVERSION_VERSION}/gitversion-alpine.3.10-x64-${GITVERSION_VERSION}.tar.gz" &&\
    echo "$url" &&\
    wget -O - $url | tar xz -C /usr/local/bin &&\
    chmod a+rx $fname
RUN echo "Testing gitversion" &&\
    apk add git tzdata libstdc++ libintl &&\
    export DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=true &&\
    git init test &&\
    cd test &&\
    git config user.name "john doe" &&\
    git config user.email "johndoe@example.com" &&\
    git commit -a -m test --allow-empty &&\
    gitversion



#FROM alpine as build-libgit2
#RUN --mount=type=cache,target=/etc/apk/cache \
#    --mount=type=cache,target=/var/cache/apk \
#    apk add bash git build-base cmake openssl-dev ncurses
#RUN git clone --recursive https://github.com/libgit2/libgit2sharp.nativebinaries &&\
#    cd libgit2sharp.nativebinaries &&\
#    git reset --hard 96eb4c0 &&\
#    git submodule init &&\
#    git submodule update
#RUN cd /libgit2sharp.nativebinaries/ &&\
#    bash -c "RID=alpine.3.11-x64 TERM=xterm ./build.libgit2.sh" &&\
#    mkdir /app && cp -v libgit2/build/*.so* /app/

#FROM mcr.microsoft.com/dotnet/core/sdk:3.1-alpine as build-gitversion
#WORKDIR /work
#RUN --mount=type=cache,target=/etc/apk/cache \
#    --mount=type=cache,target=/var/cache/apk \
#    apk add git
#RUN git clone https://github.com/GitTools/GitVersion.git
#COPY --from=build-libgit2 /app/ /opt/gitversion/
#RUN --mount=type=cache,target=/root/ \
#    ls -la ~/ &&\
#    cd GitVersion/src/GitVersionExe &&\
#    dotnet publish -f netcoreapp3.1 -r linux-musl-x64 --self-contained -o /opt/gitversion/ \
#      /p:PackageVersion_LibGit2Sharp=0.27.0-preview-0034 \
#      /p:PackageVersion_LibGit2Sharp_NativeBinaries=2.0.298 \
#      -v n
#ENV LD_LIBRARY_PATH=/opt/gitversion:${LD_LIBRARY_PATH}
#ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=true
#RUN echo "Testing execution..." &&\
#    ln -s /opt/gitversion/gitversion /usr/local/bin/ &&\
#    cd GitVersion && gitversion

FROM alpine as gitversion
RUN --mount=type=cache,target=/etc/apk/cache \
    --mount=type=cache,target=/var/cache/apk \
    apk add git tzdata libstdc++ libintl &&\
    ln -s /opt/gitversion/gitversion /usr/local/bin/
ENTRYPOINT gitversion
ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=true
COPY --from=download-gitversion /usr/local/bin/ /usr/local/bin/

FROM redis:alpine as redis
ENTRYPOINT [ "entrypoint.sh" ]
CMD [ "start" ]
ARG CITOOLS_HOME
RUN --mount=type=cache,target=/var/cache/apk \
    --mount=type=cache,target=/etc/apk/cache \
    apk add bash bind-tools &&\
    ln -s ${CITOOLS_HOME}/redis.sh /usr/local/bin/ &&\
    ln -s ${CITOOLS_HOME}/redis.sh /usr/local/bin/entrypoint.sh
COPY bin/redis.sh ${CITOOLS_HOME}/


FROM alpine as router
ENTRYPOINT [ "entrypoint.sh" ]
CMD [ "start" ]
ARG CITOOLS_HOME
RUN --mount=type=cache,target=/var/cache/apk \
    --mount=type=cache,target=/etc/apk/cache \
    apk add bash dnsmasq haproxy openssl &&\
    ln -s ${CITOOLS_HOME}/pki.sh /usr/local/bin/ &&\
    ln -s ${CITOOLS_HOME}/router.sh /usr/local/bin/ &&\
    ln -s ${CITOOLS_HOME}/router.sh /usr/local/bin/entrypoint.sh
COPY bin/router.sh bin/pki.sh ${CITOOLS_HOME}/

FROM alpine as runner
ENTRYPOINT [ "entrypoint.sh" ]
CMD [ "start" ]
ARG CITOOLS_HOME
ARG CHARTS_HOME=/var/lib/helm/charts
ENV CITOOLS_HOME=${CITOOLS_HOME}
ENV CHARTS_HOME=${CHARTS_HOME}
ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=true
RUN --mount=type=cache,target=/var/cache/apk \
    --mount=type=cache,target=/etc/apk/cache \
    echo -e "\nhttp://dl-cdn.alpinelinux.org/alpine/edge/main" >> /etc/apk/repositories &&\
    echo -e "\nhttp://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories &&\
    echo -e "\nhttp://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories &&\
    apk update &&\
    apk add bash curl git jq vim tzdata &&\
    apk add py3-pip && pip3 install yq j2cli[yaml] &&\
    apk add libstdc++ libintl &&\
    apk add coreutils findutils ncurses util-linux chrony &&\
    apk add psmisc htop &&\
    apk add podman &&\
    apk add sudo && echo "user ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/user &&\
    adduser -D user &&\
    chown -R user:user /etc/crontabs /etc/periodic /var/spool/cron &&\
    ln -s ${CITOOLS_HOME}/runner.sh /usr/local/bin/ &&\
    ln -s ${CITOOLS_HOME}/runner.sh /usr/local/bin/entrypoint.sh
COPY --from=gitversion /usr/local/bin/ /usr/local/bin/
COPY --from=download-kubectl /usr/local/bin/ /usr/local/bin/
COPY --from=download-helm /usr/local/bin/ /usr/local/bin/
#COPY --from=download-yq3 /usr/local/bin/ /usr/local/bin/
#COPY --from=download-ytt /usr/local/bin/ /usr/local/bin/
COPY --from=download-gitlab-runner /usr/local/bin/ /usr/local/bin/
COPY bin/runner.sh ${CITOOLS_HOME}/
COPY ./ ${CHARTS_HOME}/citools/
#RUN find ${CHARTS_HOME}/ && exit 1
USER user


FROM moby/buildkit:v${BUILDKIT_VERSION}-rootless as buildkit
ENTRYPOINT [ "entrypoint.sh" ]
CMD [ "start" ]
USER root
ARG CITOOLS_HOME
ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=true
RUN --mount=type=cache,target=/var/cache/apk \
    --mount=type=cache,target=/etc/apk/cache \
    echo -e "\nhttp://dl-cdn.alpinelinux.org/alpine/edge/main" >> /etc/apk/repositories &&\
    echo -e "\nhttp://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories &&\
    echo -e "\nhttp://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories &&\
    apk update &&\
    apk add bash git jq tzdata &&\
    apk add py3-pip && pip3 install yq j2cli[yaml] &&\
    apk add libstdc++ libintl &&\
    apk add curl coreutils findutils ncurses util-linux chrony &&\
    apk add psmisc htop &&\
    apk add openssl ca-certificates &&\
    apk add podman &&\
    apk add sudo && echo "user ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/user &&\
    ln -s ${CITOOLS_HOME}/buildkit.sh /usr/local/bin/ &&\
    ln -s ${CITOOLS_HOME}/buildkit.sh /usr/local/bin/entrypoint.sh
COPY --from=gitversion /usr/local/bin/ /usr/local/bin/
COPY --from=download-kubectl /usr/local/bin/ /usr/local/bin/
COPY --from=download-helm /usr/local/bin/ /usr/local/bin/
#COPY --from=download-yq3 /usr/local/bin/ /usr/local/bin/
#COPY --from=download-ytt /usr/local/bin/ /usr/local/bin/
COPY bin/buildkit.sh ${CITOOLS_HOME}/
USER user

