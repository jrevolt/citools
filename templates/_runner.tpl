{{- define "config.toml" -}}
concurrent = 10
check_interval = 1
listen_address = "0.0.0.0:9252"

[[runners]]
name = "CIv4"
url = "{{ .Values.gitlab.url }}"
clone_url = "{{ .Values.gitlab.url }}"
token = "{{ .Values.runner.token }}"
executor = "custom"
#builds_dir = "/builds"
#cache_dir = "/cache"
  [runners.custom]
  config_exec = "entrypoint.sh"
  config_args = [ "custom", "config" ]
  prepare_exec = "entrypoint.sh"
  prepare_args = [ "custom", "prepare" ]
  run_exec = "entrypoint.sh"
  run_args = [ "custom", "run" ]
  cleanup_exec = "entrypoint.sh"
  cleanup_args = [ "custom", "cleanup" ]
{{- end -}}
