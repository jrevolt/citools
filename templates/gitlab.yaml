{{- if .Values.gitlab.enabled }}
{{- $name := .Values.gitlab.name }}
{{- $namespace := .Values.k8s.namespace }}
{{- $values := .Values}}
{{- with .Values.gitlab }}

---
apiVersion: v1
kind: ConfigMap
metadata:
  name: gitlab
data:
  gitlab.rb: |
    external_url '{{ .url }}'
    nginx['listen_port'] = 80
    nginx['listen_https'] = false
    prometheus['enable'] = false
    prometheus['monitor_kubernetes'] = false
    alertmanager['enable'] = false

    #redis['enable'] = false
    #gitlab_rails['redis_host'] = 'redis'
    #gitlab_rails['redis_port'] = 6379

    postgresql['enable'] = false
    gitlab_rails['db_adapter'] = 'postgresql'
    gitlab_rails['db_encoding'] = 'utf8'
    gitlab_rails['db_host'] = '{{ $values.postgres.name }}'
    gitlab_rails['db_port'] = 5432
    gitlab_rails['db_username'] = '{{ $values.postgres.username }}'
    gitlab_rails['db_password'] = '{{ $values.postgres.password }}'

    {{- if $values.ldap.enabled }}
    gitlab_rails['ldap_enabled'] = true
    gitlab_rails['ldap_servers'] = {
      'main' => {
        'label' => 'LDAP',
        'host' =>  '{{$values.ldap.host}}',
        'port' => {{$values.ldap.port}},
        'uid' => 'sAMAccountName',
        'bind_dn' => '{{$values.ldap.dn}}',
        'password' => '{{$values.ldap.password}}',
        'encryption' => '{{$values.ldap.encryption}}',
        'timeout' => 10,
        'active_directory' => true,
        'allow_username_or_email_login' => false,
        'block_auto_created_users' => false,
        'base' => '{{$values.ldap.baseDN}}',
      }
    }
    {{- end }}

---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{.name}}
  labels:
    app: {{.name}}
spec:
  serviceName: {{.name}}
  replicas: 1
  selector:
    matchLabels:
      app: {{.name}}
  template:
    metadata:
      labels:
        app: {{.name}}
    spec:
      containers:
      - name: {{.name}}
        image: {{.image}}
        #command: [ bash ]
        #tty: true
        resources:
          requests:
            memory: 4Gi
          limits:
            memory: {{.memory}}
        volumeMounts:
        - mountPath: /var/opt/gitlab
          name: gitlab
          subPath: data
        - mountPath: /etc/gitlab
          name: gitlab
          subPath: cfg
        - mountPath: /etc/gitlab/gitlab.rb
          name: config
          subPath: gitlab.rb
      volumes:
        - name: config
          configMap:
            name: {{.name}}
        - name: gitlab
          persistentVolumeClaim:
            claimName: gitlab

---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: {{.name}}
  annotations:
    helm.sh/resource-policy: keep
spec:
  storageClassName: {{ $values.k8s.storageClassName }}
  resources:
    requests:
      storage: {{.storage}}
  accessModes:
    - ReadWriteOnce


---
apiVersion: v1
kind: Service
metadata:
  name: {{.name}}
  labels:
    app: {{.name}}
spec:
  type: ClusterIP
  ports:
  - port: 80
  selector:
    app: {{.name}}

---
apiVersion: v1
kind: Service
metadata:
  name: {{.name}}-ssh
  labels:
    app: {{.name}}
spec:
  type: NodePort
  ports:
  - port: 22
    nodePort: 30022
  selector:
    app: {{.name}}

---
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: {{.name}}
  annotations:
    {{- if eq $values.letsencrypt.kind "ClusterIssuer" }}
    cert-manager.io/cluster-issuer: {{ $values.letsencrypt.issuer }}
    {{- else -}}
    cert-manager.io/issuer: {{ $values.letsencrypt.issuer }}
    {{- end }}
    nginx.ingress.kubernetes.io/proxy-body-size: "0"
spec:
  rules:
  - host: {{ (urlParse .url).hostname }}
    http:
      paths:
      - backend:
          serviceName: {{.name}}
          servicePort: 80
  tls:
  - hosts:
    - "{{$values.k8s.domain}}"
    - "*.{{$values.k8s.domain}}"
    secretName: {{ $values.k8s.domain | replace "." "-" }}-tls


{{- end }}
{{- end }}
