{{- define "docker.config.json.yaml" -}}
{{- $values := .Values -}}
auths:
  {{- range $values.docker.auth }}
  {{.name}}:
    auth: "{{ print .user ":" .pass | b64enc }}"
  {{- end }}
{{- end -}}

{{- define "docker.config.json" -}}
{{- $values := .Values -}}
{"auths":{
{{- range $i, $e := $values.docker.auth }}
{{- if gt $i 0 }},{{end}}
"{{$e.name}}": { "auth": "{{ print $e.user ":" $e.pass | b64enc }}" }
{{- end }}
}}
{{- end -}}
