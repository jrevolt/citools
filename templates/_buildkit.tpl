{{- define "buildkit.yaml" -}}
gitlab:
  url: {{ .Values.gitlab.url }}
  token: {{ .Values.gitlab.token }}
buildkit:
  enabled: true
  image: {{ .Values.buildkit.image }}
  nameserver: {{ .Values.router.enabled | ternary  .Values.router.serviceIP .Values.k8s.dnsIP }}
  cacerts: |
    {{- .Files.Get "etc/cacerts.pem" | nindent 4 }}
    {{- .Files.Get "etc/letsencrypt.pem" | nindent 4 }}
  kubeconfig: |
    {{- .Files.Get .Values.k8s.kubeconfig | nindent 4 }}
  dockercfg: |
    {{- include "docker.config.json" $ | nindent 4  }}
{{- end -}}

