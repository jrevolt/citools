{{- define "dnsmasq.conf" -}}
{{- $values := .Values -}}
server={{ $values.k8s.dnsIP }}
{{- range .Values.docker.registries }}
host-record={{ .name }},{{ $values.router.serviceIP }}
{{- end }}
{{- end -}}

{{- define "openssl.conf" -}}
[tls]
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth,clientAuth
subjectAltName = @alt_names

[alt_names]
{{- range $i, $e := .Values.docker.registries }}
DNS.{{$i}} = {{ $e.name }}
{{- end }}
{{- end -}}

{{- define "haproxy.cfg" -}}
{{- $values := .Values -}}
global
  log stdout format raw local0
  tune.ssl.default-dh-param 2048
  resolvers mydns
    nameserver dflt {{ .Values.k8s.dnsIP }}:53

defaults
  log global
  mode http
  timeout connect 30s
  timeout client 120s
  timeout server 120s
  default-server init-addr none resolvers mydns

frontend mirror
  bind 0.0.0.0:80
  bind 0.0.0.0:443 ssl crt /etc/haproxy/tls.pem
  option httplog
{{- range .Values.docker.registries }}
  use_backend {{ .name }} if { hdr(Host) -i {{ .name }} }
{{- end }}

{{ range .Values.docker.registries }}
  {{- $url := urlParse (.url | default $values.docker.mirror) -}}
  {{- $secure := eq $url.scheme "https" -}}
  {{- $port := regexReplaceAll "[^:]+(:(\\d+))?" $url.host "$2" | default (ternary 443 80 $secure)  -}}
backend {{ .name }}
  http-request set-header Host {{ $url.hostname }}
  server {{ .name }} {{ $url.hostname }}:{{ $port }} check {{ if $secure -}} ssl verify none {{- end }}

{{ end }}
{{- end -}}

