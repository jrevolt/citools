#!/bin/bash
set -u

template=${template:-/etc/redis/nodes.conf.template}

start() {
  trap "killall redis-server" EXIT
  reconfigure
  redis-server /etc/redis/redis.conf
}

reconfigure() {
  local hostnames=$(cat $template | grep redis- | sed 's/^[^ ]* //; s/:.*//')
  local ips=""
  local regexp=""
  for i in $hostnames; do
    local h
    h="${i}.$(hostname -d)"
    echo "Resolving IP for: ${h}"
    local ip=""
    while [[ "$ip" == "" ]]; do
      ip=$(dig +noall +answer ${h} | awk '{print $5}')
      [[ "$ip" == "" ]] && sleep 1s
    done
    ips="${ip};${ips}"
    regexp="s/${i}/${ip}/;${regexp};"
  done
  cat ${template} | sed "/${POD_NAME}/ s/master/myself,master/" | sed "$regexp" | tee /data/nodes.conf
}

watchdog() {
  local ipconf=""
  while true; do
    local hostnames=$(cat $template | grep redis- | sed 's/^[^ ]* //; s/:.*//')
    local ips=""
    local regexp=""
    for i in $hostnames; do
      local ip=$(dig +noall +answer ${i}.redis.ci.svc.cluster.local | awk '{print $5}')
      ips="${ip};${ips}"
      regexp="s/${i}/${ip}/;${regexp}"
    done
    echo "ips=$ips, ipconf=$ipconf"
    if [[ "$ips" != "$ipconf" ]]; then
      killall redis-server || true
      ipconf="$ips"
      cat ${template} | sed "/${POD_NAME}/ s/master/myself,master/" | sed "$regexp" > /data/nodes.conf
      redis-server /etc/redis/redis.conf &
    fi
    sleep 10s
  done
}

"$@"
