#!/bin/bash
set -u

if ${CUSTOM_ENV_CIX_DEBUG:-false}; then echo "### $0 $@" >&2; fi

log() { echo "## $@"; }

withlog() { echo "$@"; "$@"; }

fail() {
  echo "[ERROR]($?) $@" >&2
  exit 1
}

# container entry point
start() {
  trap "killall crond gitlab-runner" EXIT
  #crond -f -l8 &
  gitlab-runner run
}

# custom gitlab entry point
custom() {
  resolve_group_project_repo || fail
  export CIX_BUILDKIT_NAME="$(get_buildkit_name)" || fail
  export CIX_K8S_NAMESPACE="$(get_k8s_namespace)" || fail
  "$@"
}

config() {
cat <<EOF
{
"builds_dir": "${HOME}/builds",
"cache_dir": "${HOME}/cache",
"builds_dir_is_shared": false
}
EOF
}

prepare() {
  prepare_buildkit
}

run() {
  eval run_${2} "$1"
}

###

runscript() {
  local script="$1"
  local name=${CIX_BUILDKIT_NAME}
  #local pod=$(kubectl get pod -l app=${name} -o name)
  if ${CUSTOM_ENV_CIX_TTY:-false}; then
    tar c -C / "${script:1}" | kubectl exec -i sts/${name} -- bash -c "tar x -C / ; mkdir -p '${CUSTOM_ENV_CI_PROJECT_DIR}'"
    script -q -c "kubectl exec -it -n ${CIX_K8S_NAMESPACE} sts/${name} -- ${script}"
  else
    tar c -C / "${script:1}" | kubectl exec -i sts/${name} -- bash -c \
      "tar x -C / ; mkdir -p '${CUSTOM_ENV_CI_PROJECT_DIR}' ; stdbuf -oL -eL ${script}"
  fi
}

run_prepare_script() { return 0; }
run_get_sources() { return 0; }
run_restore_cache() { return 0; }
run_download_artifacts() { return 0; }
run_build_script() { runscript "$@"; }
run_after_script() { return 0; }
run_archive_cache() { return 0; }
run_upload_artifacts_on_success() { return 0; }
run_upload_artifacts_on_failure() { return 0; }

###

resolve_group_project_repo() {
  # CI_PROJECT_PATH=group/subgroup/project/repo => [group/subgroup, project, repo]
  IFS=' ' read -r -a parsed <<< "$(echo ${CUSTOM_ENV_CI_PROJECT_PATH} | sed -re 's/\/([^/]+)\/([^/]+)$/ \1 \2/')"
  local group="${parsed[0]}"
  local project="${parsed[1]}"
  local repo="${parsed[2]}"
  export CIX_GITLAB_GROUP=${group}
  export CIX_GITLAB_PROJECT=${project}
  export CIX_GITLAB_REPO=${repo}
}

get_buildkit_name() {
  local default_pattern='buildkit-${CUSTOM_ENV_CI_PROJECT_ID}-${CUSTOM_ENV_CI_COMMIT_REF_SLUG}'
  local pattern="${CUSTOM_ENV_CIX_BUILDKIT_NAME_PATTERN:-$default_pattern}"
  eval echo "${pattern}"
}

get_k8s_namespace() {
  local default_pattern='${CIX_GITLAB_GROUP}-${CIX_GITLAB_PROJECT}'
  local pattern="${CUSTOM_ENV_CIX_K8S_NAMESPACE_PATTERN:-$default_pattern}"
  eval echo "${pattern}"
}

kubectl() {
  $(which kubectl) -n ${CIX_K8S_NAMESPACE} "$@"
}

is_buildkit_ready() {
  kubectl exec sts/${CIX_BUILDKIT_NAME} -- hostname &>/dev/null && return 0 || return 1
}

install_buildkit() {
    withlog helm upgrade --install --force \
      ${CIX_BUILDKIT_NAME} ${CHARTS_HOME}/citools \
      -n ${CIX_K8S_NAMESPACE} \
      -f ~/.gitlab-runner/buildkit.yaml \
      --set buildkit.name=${CIX_BUILDKIT_NAME} \
      --set buildkit.namespace=${CIX_K8S_NAMESPACE} \
      --set buildkit.cpu=${CUSTOM_ENV_CIX_BUILDKIT_CPU:-1} \
      --set buildkit.memory=${CUSTOM_ENV_CIX_BUILDKIT_MEMORY:-1Gi} \
      --set buildkit.storage=${CUSTOM_ENV_CIX_BUILDKIT_STORAGE:-5Gi} \
      --set buildkit.CI_PROJECT_ID=${CUSTOM_ENV_CI_PROJECT_ID} \
      --set buildkit.CI_PROJECT_PATH=${CUSTOM_ENV_CI_PROJECT_PATH} \
      --set buildkit.CI_COMMIT_REF_NAME=${CUSTOM_ENV_CI_COMMIT_REF_NAME} \
      --set buildkit.CI_COMMIT_REF_SLUG=${CUSTOM_ENV_CI_COMMIT_REF_SLUG} \
      "$@"
}

prepare_buildkit() {
  local name="${CIX_BUILDKIT_NAME}"
  local namespace="${CIX_K8S_NAMESPACE}"

  if is_buildkit_ready; then
    echo "sts/${name} is ready!"
    return 0
  fi

  local fname=/tmp/${name}
  (
    flock -x 200

    # this should not be needed, really; however, sometimes the release is stuck in "uninstalling", and a manual
    # intervention is needed
    helm uninstall -n ${namespace} ${name} &>/dev/null

    install_buildkit

  ) 200> ${fname} || fail

  while ! is_buildkit_ready; do
    echo "waiting for sts/${name}..."
    sleep 1s
  done
}

cleanup() {
  return 0
}

# optional; used to override and/or customize default behavior
eval "${CUSTOM_ENV_CIX_RUNNER_CUSTOMIZE:-}"

"$@"
