#!/bin/bash
set -u

START_PAUSED=${START_PAUSED:-false}
TIMEOUT_STANDBY=${TIMEOUT_STANDBY:-3m}
TIMEOUT_UNDEPLOY=${TIMEOUT_UNDEPLOY:-4h}
NIGHMODE_HOURS=${NIGHMODE_HOURS:-1900:0700}
#REGISTRY_URL=${REGISTRY_URL}

start() {

  echo "START_PAUSED=${START_PAUSED}. TIMEOUT_STANDBY=${TIMEOUT_STANDBY}, TIMEOUT_UNDEPLOY=${TIMEOUT_UNDEPLOY}, NIGHMODE_HOURS=${NIGHMODE_HOURS}"

  trap "killall buildkitd haproxy" EXIT
  if ${START_PAUSED}; then
    down
  else
    up
  fi

  watchdog &

  while true; do
    [ -f ~/.pid ] && read pid < ~/.pid

    # shouldn't be running but IS
    if [ -f ~/.paused ] && kill -0 ${pid:-} &>/dev/null; then down; fi

    # should be running but IS NOT
    if [ ! -f ~/.paused ] && ! kill -0 ${pid:-} &>/dev/null; then up; fi

    sleep 1s
  done
}

seconds() {
  local x=$1
  x=${x//h/*60*60}
  x=${x//m/*60}
  x=${x//s/}
  echo "$x"
}

stime() {
  local t=$1
  local r=0
  local x="s"
  if (( $t >= 60 )); then r=$((t%60)); t=$((t/60)); x="m"; fi
  if (( $t >= 60 )); then r=$((t%60)); t=$((t/60)); x="h"; fi
  x="${t}${x}"
  if (( r>0 )); then x="${x}${r}s"; fi
  echo $x
}

up() {
  [ -f ~/.pid ] && echo "Already running?" && return 0
  [ -f ~/.paused ] && rm ~/.paused
  rootlesskit buildkitd & echo -n $! > ~/.pid
  while [ ! -S ${BUILDKIT_HOST//unix:} ]; do
    echo "...waiting for buildkitd"
    sleep 1s
  done
}

down() {
  touch ~/.paused
  [ -f ~/.pid ] && read pid < ~/.pid
  if [ "${pid:-}" != "" ]; then
    kill ${pid} &>/dev/null
    timeout 5s sh -c "wait ${pid}" || kill -9 ${pid} &>/dev/null || true
  fi
  [ -f ~/.pid ] && rm ~/.pid
  [ -S ${BUILDKIT_HOST//unix:} ] && rm -f ${BUILDKIT_HOST//unix:}
}

client() {
  [ -f ~/.paused ] && rm ~/.paused
  while [ ! -S ${BUILDKIT_HOST//unix:} ]; do
    echo "...waiting for buildkitd"
    sleep 1s
  done
}

watchdog() {
  while true; do
    check_pipeline
    sleep 10s
  done
}

check_pipeline() {
  source <(
    curl -s -H "Private-Token: ${CIX_GITLAB_TOKEN}" \
      "${CIX_GITLAB_URL}/api/v4/projects/${CI_PROJECT_ID}/pipelines?scope=branches&ref=${CI_COMMIT_REF_NAME}" \
      | jq -r '.[0] | "pipeline="+.status+"; timestamp="+.updated_at'
  )

  local active=true

  case ${pipeline} in
    created|pending|running)
      active=true
      age=0
      ;;
    success|failed|canceled|skipped)
      (( age=$(date +%s)-$(date +%s --date=${timestamp}) ))
      active=false
      ;;
    *)
      active=false
      age=-1
      ;;
  esac

  local standby=$(( `seconds ${TIMEOUT_STANDBY}` ))
  local undeploy=$(( `seconds ${TIMEOUT_UNDEPLOY}` ))
  local nightmode=${NIGHMODE_HOURS}
  local nightmode_start=${nightmode//:*/}
  local nightmode_end=${nightmode//*:/}
  local now=$(date +%H%M)

  local status="undefined"
  [ -f ~/.paused ] && status="standby" && [ -f ~/.size ] && read size < ~/.size
  [ -f ~/.pid ] && { read pid < ~/.pid; kill -0 ${pid} &>/dev/null && status="active" && size=; }

  echo "status=${status}, pipeline=${pipeline}, age=$(stime $age), standby=${TIMEOUT_STANDBY}, undeploy=${TIMEOUT_UNDEPLOY}, nightmode_start=${nightmode_start}, nightmode_end=${nightmode_end}, size=${size:-}"

  ${active} && return 0

  if (( $age == -1 )); then
    echo "Undeploying obsolete worker. Looks like the associated branch has been removed."
    cleanup_registry
    undeploy
    exit 0
  fi

  if [[ "$now" > "$nightmode_start" || "$now" < "$nightmode_end" ]] && (( $age > $standby )); then
    echo "Night mode (${nightmode_start}-${nightmode_end}). Undeploying worker..."
    undeploy
    exit 0
  fi

  if (( $age > ${undeploy} )); then
    echo "Undeploying unused worker. Age: ${age} > ${undeploy}"
    undeploy
    exit 0
  fi

  if (( $age > ${standby} )) && [ "$status" = "active" ]; then
    echo "Standing by. Stopping Buildkit daemon. Age: ${age} > ${standby}"
    down
    du -sh ~/.local/share/buildkit | awk '{print $1}' > ~/.size
    sudo bash -c "echo 1 > /proc/sys/vm/drop_caches"
    return 0
  fi

  return 0
}

undeploy() {
  helm uninstall --no-hooks --keep-history -n ${K8S_NAMESPACE} ${K8S_DEPLOYMENT}
}

cleanup_registry() {
  if [ ! -f ~/.pid ]; then
    rootlesskit buildkitd & pid=$!
    trap "kill ${pid}" EXIT
    while [ ! -S ${BUILDKIT_HOST//unix:} ]; do
      echo "...wating for buildkitd"
      sleep 1s
    done
  fi
  local registry=registry.dev.jrevolt.io
  local tag=${CI_COMMIT_REF_SLUG}
  local image=${CI_PROJECT_PATH}
  local cache=${CI_PROJECT_PATH}/cache
  cleanup_registry0 ${image}

  # cache is tricky, registry won't let us delete it:
  # overwrite cache tag as empty image, then delete
  (
    mkdir -p /tmp/empty &&
    cd /tmp/empty &&
    echo "FROM scratch" > /tmp/empty/Dockerfile &&
    buildctl build --frontend dockerfile.v0 \
      --local context=. --local dockerfile=. \
      --output type=image,name=${registry}/${cache}:${tag},push=true
  )
  cleanup_registry0 ${cache}
}

cleanup_registry0() {
  local image=$1
  local registry=${REGISTRY_URL}
  local tag=${CI_COMMIT_REF_SLUG}
  echo "Cleaning up registry: ${image}:${tag}"
  local hash=$(curl -sSLI "${registry}/v2/${image}/manifests/${tag}" \
    -H 'Connection: close' \
    -H 'Accept: application/vnd.docker.distribution.manifest.v2+json' \
    | awk '$1 == "docker-content-digest:" { print $2 }' | tr -d $'\r')
  echo "hash=$hash"
  if [[ "$hash" != "" ]]; then
    curl -si -XDELETE "${registry}/v2/${image}/manifests/${hash}" | head -n1 && echo "[success]" || echo "[failed]"
  fi
  #kubectl exec -n ${K8S_NAMESPACE} deployment/registry -- registry garbage-collect /etc/docker/registry/config.yml -m
  return 0
}

"$@"
