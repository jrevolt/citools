#!/bin/bash
set -u

ORG="/O=JRevolt/OU=Research & Development/L=Bratislava/C=SK"
DOMAIN="/DC=cloud/DC=local"
CA_ROOT="/CN=Cloud CA Root"
CA_ISSUING="/CN=Cloud CA Issuing"
TLS="/CN=*.cloud.local"

KEYSIZE=2048

key() {
	local name="$1"
	[ -f ${name}.key ] || openssl genrsa -out ${name}.key $KEYSIZE
}

csr() {
	local name="$1"
	local subj="$2"
	[ -f ${name}.csr ] || openssl req -sha256 -new -key ${name}.key -subj "${subj}" -out ${name}.csr
}

crt() {
	local name="$1"
	local signer="$2"
	local validity="$3"
	shift 3
	if [[ "$name" = "$signer" ]]; then
		[ -f ${name}.crt ] ||
		openssl x509 -req -sha256 -in ${name}.csr \
			-signkey "${signer}.key" -CAcreateserial -days $validity \
			-out ${name}.crt "$@"
	else
		[ -f ${name}.crt ] ||
		openssl x509 -req -sha256 -in ${name}.csr \
			-CA "${signer}.crt" -CAkey "${signer}.key" -CAcreateserial -days $validity \
			-out ${name}.crt "$@"
	fi
}

all() {
   	local name="$1"
   	local signer="$2"
   	local subj="$3"
   	local validity="$4"
   	shift 4
   	key "$name"
   	csr "$name" "$subj"
   	crt "$name" "$signer" "$validity" "$@"
   	ls -la ${name}.{key,crt}
}

ca() {
	local name="ca"
	local subj="${CA_ROOT}${DOMAIN}${ORG}"
	all "$name" "$name" "$subj" 9999 -extensions ${name} -extfile openssl.conf
	cat ca*crt > cacerts.pem
}

ca1() {
  ca
	local name="ca1"
	local subj="${CA_ISSUING}${DOMAIN}${ORG}"
	all "$name" "ca" "$subj" 3650 -extensions ${name} -extfile openssl.conf
	cat ca*crt > cacerts.pem
}

tls() {
  #ca
  #ca1
  local domain="tls"
  local subj="/CN=${domain}${DOMAIN}${ORG}"
  all "$domain" "ca1" "$subj" 999 -extensions tls -extfile openssl.conf
}


"$@"
