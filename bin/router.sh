#!/bin/bash

fail() { echo "[ERR] $@"; exit 1; }

start() {
  tls &&
  dnsmasq &&
  haproxy -f /etc/haproxy/haproxy.cfg &&
  trap "killall dnsmasq haproxy" EXIT &&
  wait
}

tls() {
  cd /etc/router &&
  pki.sh tls &&
  cat tls.key tls.crt cacerts.pem > /etc/haproxy/tls.pem &&
  echo "TLS certificate generated" ||
  fail "Error generating TLS certificate!"
}

check() {
  return 0
}

"$@"

